class AddImageToTestimonials < ActiveRecord::Migration[5.0]
  def change
    add_attachment :testimonials, :image
  end
end
