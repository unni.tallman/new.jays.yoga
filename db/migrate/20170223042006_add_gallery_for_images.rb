class AddGalleryForImages < ActiveRecord::Migration[5.0]
  def change
    add_column :images, :gallery, :boolean, default: false
  end
end
