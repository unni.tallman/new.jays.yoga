class AddFlickrUrlToImages < ActiveRecord::Migration[5.0]
  def change
    add_column :images, :flickr_url, :text
  end
end
