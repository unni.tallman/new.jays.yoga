class AddShortDescriptionForImages < ActiveRecord::Migration[5.0]
  def change
    add_column :images, :short_description, :text, default: ''
  end
end
