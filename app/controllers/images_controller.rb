class ImagesController < ApplicationController
  def show
    @image = Image.find_by_slug(params[:id])

    @share_image = @image.file.url
    @share_title = @image.title
    @share_description = @image.short_description.present? ? @image.short_description : nil
    @share_url   = image_url(@image)
  end
end
