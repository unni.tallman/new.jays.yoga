class TestimonialsController < ApplicationController
  def show
    @testimonial = Testimonial.find_by_slug(params[:id])
  end
end
