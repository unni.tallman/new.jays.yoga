class HomeController < ApplicationController
  layout :false
  
  def index
    @testimonials = Testimonial.all
    @images       = Image.for_gallery.random.limit(3)
  end
end