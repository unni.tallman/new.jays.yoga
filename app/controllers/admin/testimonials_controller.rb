class Admin::TestimonialsController < Admin::ApplicationController
  before_filter :set_active_tab
  before_filter :find_testimonial, except: [:index, :new, :create]

  def index
    @testimonials = Testimonial.all
  end

  def create
    @testimonial = Testimonial.new(testimonial_params)
    if @testimonial.save
      redirect_to admin_testimonials_path
    else
      render 'new'
    end
  end

  def new
    @testimonial = Testimonial.new
  end

  def update
    if @testimonial.update_attributes(testimonial_params)
      redirect_to admin_testimonials_path
    else
      render 'edit'
    end
  end

  def edit
  end

  def destroy
    @testimonial.destroy
    redirect_to admin_testimonials_path
  end

  private

  def find_testimonial
    @testimonial = Testimonial.find_by_slug(params[:id])
  end

  def testimonial_params
    params.require(:testimonial).permit(:name, :title, :description, :image)
  end

  def set_active_tab
    @active_tab = "Testimonials"
  end
end