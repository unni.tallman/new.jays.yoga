class Admin::ImagesController < Admin::ApplicationController
  before_filter :set_active_tab
  before_filter :find_image, except: [:index, :new, :create]

  def index
    @images = Image.all
  end

  def create
    @image = Image.new(image_params)
    if @image.save
      redirect_to admin_images_path
    else
      render 'new'
    end
  end

  def new
    @image = Image.new
  end

  def update
    if @image.update_attributes(image_params)
      redirect_to admin_images_path
    else
      render 'edit'
    end
  end

  def edit
  end

  def destroy
    @image.destroy
    redirect_to admin_images_path
  end

  private

  def find_image
    @image = Image.find(params[:id])
  end

  def image_params
    params.require(:image).permit(:title, :description, :file, :short_description, :flickr_url)
  end

  def set_active_tab
    @active_tab = "Images"
  end
end