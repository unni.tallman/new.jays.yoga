class Testimonial < ApplicationRecord
  has_attached_file :image #, :styles => {original: 'unused_option'}, processors: [:tinypng]
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  def self.find_by_slug(slug)
    Testimonial.all.select{|image| image.slug == slug}.first
  end

  def slug
    name.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
  end

  def to_param
    slug
  end
end
