class Image < ApplicationRecord
  has_attached_file :file, :styles => {original: 'unused_option'}, processors: [:tinypng]
  validates_attachment_content_type :file, content_type: /\Aimage\/.*\z/
  scope :for_gallery, -> { where(gallery: true) }
  validates_presence_of :title#, :short_description, :description

  def self.random
    order("RANDOM()")
  end

  def self.find_by_slug(slug)
    Image.all.select{|image| image.slug == slug}.first
  end

  def slug
    title.present? ? title.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '') : 'dummy'
  end

  def to_param
    slug
  end

  def flickr_embed_code
    %{<object width="800" height="600"> <param name="flashvars" value="offsite=true&lang=en-us&page_show_url=#{URI.escape(flickr_show_url)}&page_show_back_url=#{URI.escape(flickr_show_back_url)}"></param> <param name="movie" value="https://www.flickr.com/apps/slideshow/show.swf?v=261948265"></param> <param name="allowFullScreen" value="true"></param><embed type="application/x-shockwave-flash" src="https://www.flickr.com/apps/slideshow/show.swf?v=261948265" allowFullScreen="true" flashvars="offsite=true&lang=en-us&page_show_url=#{URI.escape(flickr_show_url)}&page_show_back_url=#{URI.escape(flickr_show_back_url)}" width="400" height="300"></embed></object>}
  end

  def flickr_set_id
    flickr_url.split('/')[6]
  end

  def flickr_show_url
    flickr_url.gsub('https://www.flickr.com','') +  '/show'
  end

  def flickr_show_back_url
    flickr_url.gsub('https://www.flickr.com','').gsub('albums','sets') + "/&set_id=#{flickr_set_id}&jump_to="
  end
end
