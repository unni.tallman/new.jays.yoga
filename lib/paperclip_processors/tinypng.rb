require 'net/http'
require 'openssl'

module Paperclip
  class Tinypng < Processor
    def make
      basename = File.basename(file.path, File.extname(file.path))
      dst_format = options[:format] ? ".\#{options[:format]}" : ''

      dst = Tempfile.new([basename, dst_format])
      dst.binmode

      tinypng_compress File.expand_path(file.path), File.expand_path(dst.path)

      dst
    end

    def tinypng_compress(input, output)
      uri = URI.parse('https://api.tinypng.com/shrink')

      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      # http.ssl_version = :TLSv1
      # http.ciphers = ['DES-CBC3-SHA']

      request = Net::HTTP::Post.new(uri.request_uri)
      request.basic_auth('api', Rails.application.secrets.tinypng_api_key)

      response = http.request(request, File.binread(input))

      if response.code == '201'
        File.binwrite(output, http.get(response['location']).body)
      else
        puts 'Compression failed'
      end
    end

  end
end
