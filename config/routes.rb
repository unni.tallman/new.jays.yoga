Rails.application.routes.draw do
  root to: 'home#index'

  namespace :admin do 
    resources :testimonials
    resources :images
  end

  get '/:id', to: 'images#show', as: :image
  get '/t/:id', to: 'testimonials#show', as: :testimonial

  match "*path", to: 'home#index', via: :all
end